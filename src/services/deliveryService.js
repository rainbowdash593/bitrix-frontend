import statusesService from "./statusesService";

export default {
    parseCalls(calls) {
        calls.forEach(call => {
            call.phone_formatted = call.phone.phone;
            call.status_formatted = statusesService.description(call.status)
        })
        return calls;
    }
}
