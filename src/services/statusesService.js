const statuses = [
    { name: 'created', description: 'Создано'},
    { name: 'in_process', description: 'В процессе'},
    { name: 'error', description: 'Ошибка'},
    { name: 'finished', description: 'Закончено'},
    { name: 'stop', description: 'Остановлено'},
]

export default {
    get() {
        return statuses;
    },
    description(name) {
        return statuses.find(status => status.name == name).description;
    }
}
