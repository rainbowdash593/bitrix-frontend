import Axios from 'axios';
import router from '../router';
import store from '../store';


Axios.defaults.baseURL = process.env.NODE_ENV == 'development'
    ? process.env.VUE_APP_BASE_URL
    : `${location.origin}/api`;

const authToken = localStorage.getItem('token')
    ? `Bearer ${localStorage.getItem('token')}`
    : '';

Axios.defaults.headers.common.Authorization = Axios.defaults.headers.common.Authorization
    ? Axios.defaults.headers.common.Authorization
    : authToken;

Axios.interceptors.response.use(undefined, (err) => {
    const res = err.response;
    if (res.status === 401 && res.config && !res.config.__isRetryRequest) {
        return new Promise((resolve, reject) => {
            store.dispatch('REFRESH_TOKEN')
                .then((token) => {
                    err.config.__isRetryRequest = true;
                    Axios.defaults.headers.common.Authorization = `Bearer ${token}`;
                    err.config.headers.Authorization = `Bearer ${token}`;
                    resolve(Axios(err.config));
                })
                .catch(() => {
                    err.config.__isRetryRequest = true;
                    console.log('Refresh login error: ', error);
                    store.dispatch('LOGOUT');
                    router.push({ path: '/auth' });
                    reject(error);
                });
        });
    }
});

const AxiosClient = Axios.create();

export default AxiosClient;
