import Vue from 'vue'
import VueRouter from 'vue-router'
import Guard from 'vue-router-multiguard';

import middlewares from "./middlewares";

import AuthPage from '../views/Auth'
import Panel from "../views/Panel";
import SettingsPage from "../views/Dashboard/Settings";
import DeliveriesPage from "../views/Dashboard/Deliveries";
import CreateDelivery from "../views/Dashboard/CreateDelivery";
import DeliveryDetail from "../views/Dashboard/DeliveryDetail";


const routes = [
    {
        path: '/',
        redirect: '/auth'
    },
    {
        path: '/auth',
        name: 'auth',
        component: AuthPage
    },
    {
        path: '/panel',
        name: 'panel',
        component: Panel,
        beforeEnter: Guard([
            middlewares.authRequired,
        ]),
        children: [
            {
                path: 'settings',
                name: 'settings',
                component: SettingsPage,
                beforeEnter: Guard([
                    middlewares.authRequired,
                ]),
            },
            {
                path: 'deliveries',
                name: 'deliveries',
                component: DeliveriesPage,
                beforeEnter: Guard([
                    middlewares.authRequired,
                ]),
            },
            {
                path: 'delivery/create',
                name: 'delivery-create',
                component: CreateDelivery,
                beforeEnter: Guard([
                    middlewares.authRequired,
                ]),
            },
            {
                path: 'delivery/:id',
                name: 'delivery-detail',
                component: DeliveryDetail,
                beforeEnter: Guard([
                    middlewares.authRequired,
                ]),
            }
        ]
    }
]

Vue.use(VueRouter)
const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
