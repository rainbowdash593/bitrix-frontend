import store from '../store';

export default {
    authRequired(to, from, next) {
        const route = store.getters.IS_LOGGED_IN
            ? undefined
            : '/auth';
        next(route);
    },
}
