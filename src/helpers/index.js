export default {
    appendFormdata(FormData, data, name) {
        name = name || '';
        if (data instanceof File) {
            FormData.append(name, data, data.name);
        } else {
            if (typeof data === 'object') {
                for (let key in data) {
                    if (name == ''){
                        this.appendFormdata(FormData, data[key], key);
                    } else {
                        this.appendFormdata(FormData, data[key], name + '['+key+']');
                    }
                }
            } else {
                FormData.append(name, data);
            }
        }
    }
}
