import AxiosClient from '../../config/axios';

const state = {
    auth: {
        credentials: {
            login: '',
            password: '',
        },
        token: localStorage.getItem('token') || '',
        refreshToken: localStorage.getItem('refreshToken') || '',
        isLoading: false,
        status: '',
    },
    user: null,
    settings: {
        isLoading: false,
    }
};

const getters = {
    AUTH_CREDENTIALS: state => state.auth.credentials,
    AUTH_TOKEN: state => state.auth.token,
    REFRESH_TOKEN: state => state.auth.refreshToken,
    AUTH_USER: state => state.user,
    AUTH_IS_LOADING: state => state.auth.isLoading,
    SETTINGS_IS_LOADING: state => state.settings.isLoading,
    IS_LOGGED_IN: state => !!state.auth.token,
};

const mutations = {
    SET_AUTH_CREDITIONALS: (state, credentials) => {
        state.auth.credentials.login = credentials.login;
        state.auth.credentials.password = credentials.password;
    },
    AUTH_SUCCESS: (state, {token, refreshToken}) => {
        state.auth.token = token;
        state.auth.refreshToken = refreshToken;
        state.auth.status = 'success';
    },
    AUTH_FAILED: (state) => {
        state.auth.token = '';
        state.auth.authUser = {};
        state.auth.status = 'failed';
    },
    SET_AUTH_IS_LOADING: (state, isLoading) => {
        state.auth.isLoading = isLoading;
    },
    SET_AUTH_USER: (state, user) => {
        state.user = user;
    },
    AUTH_LOGOUT: (state) => {
        state.auth.token = '';
        state.auth.status = '';
        state.user = {};
    },
    UPDATE_TOKEN: (state, token) => {
        state.auth.token = token;
    },
    SET_SETTINGS_LOADING: (state, isLoading) => {
        state.settings.isLoading = isLoading
    }
};

const actions = {

    AUTH: context => new Promise((resolve, reject) => {
        context.commit('SET_AUTH_IS_LOADING', true);
        AxiosClient.post('/user/login', state.auth.credentials)
            .then((data) => {
                context.commit('AUTH_SUCCESS', {token: data.data.access_token, refreshToken: data.data.refresh_token});
                context.commit('SET_AUTH_USER', data.data.user);
                localStorage.setItem('token', data.data.access_token);
                localStorage.setItem('refreshToken', data.data.refresh_token);
                AxiosClient.defaults.headers.common.Authorization = `Bearer ${data.data.access_token}`;
                resolve(data.data.user);
            })
            .catch((error) => {
                context.commit('AUTH_FAILED');
                reject(error);
            })
            .finally(() => {
                context.commit('SET_AUTH_IS_LOADING', false);
            });
    }),

    LOGOUT: context => new Promise((resolve, reject) => {
        context.commit('AUTH_LOGOUT');
        localStorage.removeItem('token');
        localStorage.removeItem('refreshToken');
        delete AxiosClient.defaults.headers.common.Authorization;
        resolve();
    }),

    REFRESH_TOKEN: (context) => {
        context.commit('SET_AUTH_IS_LOADING', true);
        return new Promise((resolve, reject) => {
            AxiosClient.defaults.headers.common.Authorization = `Bearer ${state.auth.refreshToken}`;
            AxiosClient.post('/user/refresh')
                .then((data) => {
                    const token = data.data.token;
                    context.commit('UPDATE_TOKEN', token);
                    localStorage.setItem('token', token);
                    resolve(token);
                })
                .catch(() => {
                    reject();
                })
                .finally(() => {
                    context.commit('SET_AUTH_IS_LOADING', false);
                });
        });
    },

    GET_AUTH_USER: (context) => {
        context.commit('SET_AUTH_IS_LOADING', true);
        return new Promise((resolve, reject) => {
            AxiosClient.get('/user/auth-user')
                .then((data) => {
                    context.commit('SET_AUTH_USER', data.data.user);
                    resolve();
                })
                .catch(() => {
                    reject();
                })
                .finally(() => {
                    context.commit('SET_AUTH_IS_LOADING', false);
                });
        });
    },

    UPDATE_USER_SETTINGS: (context) => {
        context.commit('SET_SETTINGS_LOADING', true);
        return new Promise((resolve, reject) => {
            AxiosClient.post('/user/update', context.state.user)
                .then(() => {
                    resolve();
                })
                .catch(() => {
                    reject();
                })
                .finally(() => {
                    context.commit('SET_SETTINGS_LOADING', false);
                })
        })
    }

};

export default {
    state,
    getters,
    mutations,
    actions,
};
