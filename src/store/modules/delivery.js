import AxiosClient from '../../config/axios';
import Helpers from '../../helpers/index'
import deliveryService from "../../services/deliveryService";

const state = {
    delivery: {
        file: null,
        parsedRows: []
    },
    isLoading: false,
    previewModalOpened: false,
    phoneBase: {
        mapping: {}
    },
    outgoingPhones: {
        phones: [],
        isLoading: false
    },
    deliveries: [],
    currentDelivery: {
        info: {},
        calls: {
            calls: [],
            total: 0
        }
    }
};

const getters = {
    DELIVERY: state => state.delivery,
    DELIVERY_IS_LOADING: state => state.isLoading,
    SHOW_PREVIEW_MODAL: state => state.previewModalOpened,
    PREVIEW_ROWS: state => state.delivery.parsedRows,
    DELIVERY_PHONE_BASE_MAPPING: state => state.phoneBase.mapping,
    PHONE_BASE_MAPPING_EXIST: state => {
        const values = Object.values(state.phoneBase.mapping);
        return !!values.filter(item => !!item).length;
    },
    OUTGOING_PHONES: state => state.outgoingPhones.phones,
    OUTGOING_PHONES_IS_LOADING: state => state.outgoingPhones.isLoading,
    DELIVERIES: state => state.deliveries,
    CURRENT_DELIVERY: state => state.currentDelivery.info,
    CURRENT_DELIVERY_CALLS: state => state.currentDelivery.calls,
};

const mutations = {
    SET_DELIVERY_IS_LOADING: (state, isLoading) => {
        state.isLoading = isLoading;
    },

    SET_PREVIEW_MODAL_SHOW: (state, show) => {
        state.previewModalOpened = show;
    },

    SET_PARSED_ROWS: (state, rows) => {
        state.delivery.parsedRows = rows;
    },

    SET_OUTGOING_PHONES: (state, outgoingPhones) => {
        state.outgoingPhones.phones = outgoingPhones;
    },

    SET_OUTGOING_PHONES_IS_LOADING: (state, isLoading) => {
        state.outgoingPhones.isLoading = isLoading;
    },

    SET_DELIVERIES: (state, deliveries) => {
        state.deliveries = deliveries;
    },

    SET_CURRENT_DELIVERY: (state, delivery) => {
        state.currentDelivery.info = delivery;
    },
    SET_CURRENT_DELIVERY_CALLS: (state, calls) => {
        state.currentDelivery.calls = calls;
    }
};

const actions = {

    BASE_PREVIEW: (context) => {
        context.commit('SET_DELIVERY_IS_LOADING', true);

        let form = new FormData();
        form.append('file', context.state.delivery.file);

        const headers = {'Content-Type': 'multipart/form-data'}

        return new Promise((resolve, reject) => {
            AxiosClient.post('/deliveries/base-preview', form, {headers})
                .then((response) => {
                    context.commit('SET_PARSED_ROWS', response.data.rows)
                    resolve(response);
                })
                .catch(() => {
                    reject();
                })
                .finally(() => {
                    context.commit('SET_DELIVERY_IS_LOADING', false);
                })
        })
    },

    SHOW_PREVIEW_MODAL: (context, show) => {
        context.commit('SET_PREVIEW_MODAL_SHOW', show);
    },

    GET_OUTGOING_PHONES: context => {
        context.commit('SET_OUTGOING_PHONES_IS_LOADING', true);

        return new Promise((resolve, reject) => {
            AxiosClient.get('/zvonobot/outgoing-phones')
                .then(response => {
                    context.commit('SET_OUTGOING_PHONES', response.data.outgoing_phones);
                    resolve(response.data.outgoing_phones);
                })
                .catch(() => {
                    reject();
                })
                .finally(() => {
                    context.commit('SET_OUTGOING_PHONES_IS_LOADING', false);
                })
        })
    },

    CREATE_DELIVERY: context => {
        context.commit('SET_DELIVERY_IS_LOADING', true);

        const headers = {'Content-Type': 'multipart/form-data'}
        let params = {
            'name': context.state.delivery.name,
            'outgoing_phone': context.state.delivery.outgoing_phone,
            'file': context.state.delivery.file,
            'mapping': JSON.stringify(context.state.phoneBase.mapping)
        }
        let data = new FormData();
        Helpers.appendFormdata(data, params);

        return new Promise((resolve, reject) => {
            AxiosClient.post('/deliveries/create', data, {headers})
                .then((response) => {
                    console.log(response);
                    resolve();
                })
                .catch(() => {
                    reject();
                })
                .finally(() => {
                    context.commit('SET_DELIVERY_IS_LOADING', false);
                })
        })
    },

    GET_DELIVERIES: context => {
        context.commit('SET_DELIVERY_IS_LOADING', true);

        return new Promise((resolve, reject) => {
            AxiosClient.get('/deliveries/get')
                .then(response => {
                    context.commit('SET_DELIVERIES', response.data.deliveries);
                    resolve(response.data.deliveries);
                })
                .catch(() => {
                    reject();
                })
                .finally(() => {
                    context.commit('SET_DELIVERY_IS_LOADING', false);
                })
        })
    },

    GET_DELIVERY_DETAIL: (context, deliveryId) => {
        context.commit('SET_DELIVERY_IS_LOADING', true);

        return new Promise((resolve, reject) => {
            AxiosClient.get('/deliveries/info/' + deliveryId)
                .then(response => {
                    context.commit('SET_CURRENT_DELIVERY', response.data.delivery);
                    resolve(response.data.delivery);
                })
                .catch(() => {
                    reject();
                })
                .finally(() => {
                    context.commit('SET_DELIVERY_IS_LOADING', false);
                })
        })
    },

    GET_DELIVERY_CALLS: (context, {id, page, itemsPerPage}) => {
        context.commit('SET_DELIVERY_IS_LOADING', true);

        return new Promise((resolve, reject) => {
            AxiosClient.get('/deliveries/calls/' + id, {params: {page, items: itemsPerPage}})
                .then(response => {
                    let data = response.data;
                    deliveryService.parseCalls(data.calls);
                    context.commit('SET_CURRENT_DELIVERY_CALLS', data);
                    resolve(data);
                })
                .catch(() => {
                    reject();
                })
                .finally(() => {
                    context.commit('SET_DELIVERY_IS_LOADING', false);
                })
        })
    }

};

export default {
    state,
    getters,
    mutations,
    actions,
};
